# Docker Nginx Web Proxy
A reverse web proxy.

# How to use
1. Setup a letsencrypt nginx proxy companion. If you don't want to use it, just open 
port 80 for the proxy container then move on to step 2.
https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion

2. Start the Nginx Web Proxy server. You might need to create a network called 
`letsencrypt_net`. If you are using letsencrypt nginx proxy companion, make sure
it's running in the same Docker network.
`docker-compose up -d`

# Environment variables
- `UPSTREAM` - The destination domain you want to access.
- `VIRTUAL_HOST` - Your own host.
- `LETSENCRYPT_HOST` - Your own host.
- `LETSENCRYPT_EMAIL` - Your own email address. 
